"""cession_project URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import include, path
from cessions import views
from django_filters.views import FilterView
from cessions.models import Apartment
from cessions.forms import ApartmentFilter

from django.conf import settings
from django.conf.urls.static import static

urlpatterns = [

                  path("select2/", include("django_select2.urls")),
                  path('', views.cover_view),

                  path('admin/', admin.site.urls),
                  path('accounts/', include('django.contrib.auth.urls')),

                  # path(),

                  # accounts/login/ [name='login']
                  # accounts/logout/ [name='logout']
                  # accounts/password_change/ [name='password_change']
                  # accounts/password_change/done/ [name='password_change_done']
                  # accounts/password_reset/ [name='password_reset']
                  # accounts/password_reset/done/ [name='password_reset_done']
                  # accounts/reset/<uidb64>/<token>/ [name='password_reset_confirm']
                  # accounts/reset/done/ [name='password_reset_complete']

                  # справочные данные по застройщикам
                  path('developers', views.developers_view, name='developers'),
                  path('add_developer', views.CreateDeveloperView.as_view(), name='create_developer'),
                  path('edit_developer/<slug:pk>', views.EditDeveloperView.as_view(), name='edit_developer'),
                  path('delete_developer/<slug:pk>', views.DeleteDeveloperView.as_view(), name='delete_developer'),

                  # Жилые комплексы
                  path('residential_complexes', views.residential_complex_view, name='residential_complexes_list'),
                  path('add_residential_complex', views.CreateResidentialComplexView.as_view(), name='create_rc'),
                  path(r'edit_residential_complex/<slug:pk>', views.EditResidentialComplexView.as_view(),
                       name='edit_rc'),
                  path('delete_residential_complex/<slug:pk>', views.DeleteResidentialComplexView.as_view(),
                       name='delete_rc'),

                  # особенности (для тегирования квартир)
                  path('features', views.features_view, name='features'),
                  path('create_feature', views.CreateFeatureView.as_view(), name='create_feature'),
                  path('edit_feature/<slug:pk>', views.EditFeatureView.as_view(), name='edit_feature'),
                  path('delete_feature/<slug:pk>', views.DeleteFeatureView.as_view(), name='delete_feature'),

                  # банки (для обременения)
                  path('banks', views.banks_view, name='banks'),
                  path('add_bank', views.CreateBankView.as_view(), name='create_bank'),
                  path('edit_bank/<slug:pk>', views.EditBankView.as_view(), name='edit_bank'),
                  path('delete_bank/<slug:pk>', views.DeleteBankView.as_view(), name='delete_bank'),

                  # просмотр и ввод данных по квартирам
                  # path('apartments', views.apartment_view, name='apartments'),
                  path('apartments', views.ApartmentsListView.as_view(), name='apartments'),
                  # path('apartments', views.apartment_view, name='apartments'),
                  path('add_apartment', views.CreateApartmentView.as_view(), name='create_apartment'),
                  path('apartment_info/<slug:pk>', views.ApartmentInfoView.as_view(), name='apartment_info'),
                  path('apartment_columns', views.ApartmentsColumnVisibility.as_view(), name='apartment_columns'),
                  # path('apartment_filter', views.Filter.as_view(), name='apartment_filter'),
                  path('edit_apartment/<slug:pk>', views.EditApartmentView.as_view(), name='edit_apartment'),
                  path('delete_apartment/<slug:pk>', views.DeleteApartmentView.as_view(), name='delete_apartment'),

                  # заполнение данных по продавцам/покупателям
                  path('persons', views.person_view, name='persons'),
                  path('add_person', views.CreatePersonView.as_view(), name='create_person'),
                  path('edit_person/<slug:pk>', views.EditPersonView.as_view(), name='edit_person'),
                  path('delete_person/<slug:pk>', views.DeletePersonView.as_view(), name='delete_person'),

                  path("select2/", include("django_select2.urls")),


              ] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
