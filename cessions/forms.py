from functools import partial

from django import forms
import django_filters

from .models import Developer, ResidentialComplex, Feature, Apartment, Bank, Person
from bootstrap_modal_forms.forms import BSModalModelForm
import django_select2.forms as select2forms
from django.forms import Select as Select2


class DeveloperWidget(select2forms.ModelSelect2Widget):
    search_fields = [
        "developer__icontains",
    ]


class ApartmentFilter(django_filters.FilterSet):
    id = django_filters.NumberFilter(field_name='id', lookup_expr='icontains')
    commission__gt = django_filters.NumberFilter(field_name='commission', lookup_expr='gt')
    commission__lt = django_filters.NumberFilter(field_name='commission', lookup_expr='lt')
    developer = django_filters.ModelChoiceFilter(queryset=Developer.objects.all())
    residential_complex = django_filters.ModelChoiceFilter(queryset=ResidentialComplex.objects.all())
    queue = django_filters.NumberFilter(field_name='queue_number', lookup_expr='iexact')
    corpus = django_filters.NumberFilter(field_name='corpus', lookup_expr='iexact')
    section = django_filters.NumberFilter(field_name='section', lookup_expr='iexact')
    floor = django_filters.NumberFilter()
    floor__gt = django_filters.NumberFilter(field_name='floor', lookup_expr='gt')
    floor__lt = django_filters.NumberFilter(field_name='floor', lookup_expr='lt')
    floor_overall = django_filters.NumberFilter()
    floor_overall__gt = django_filters.NumberFilter(field_name='floor_overall', lookup_expr='gt')
    floor_overall__lt = django_filters.NumberFilter(field_name='floor_overall', lookup_expr='lt')
    deadline = django_filters.DateFilter(field_name='deadline')
    deadline__gt = django_filters.DateFilter(field_name='deadline', lookup_expr='gt')
    deadline__lt = django_filters.DateFilter(field_name='deadline', lookup_expr='lt')
    number_of_rooms = django_filters.NumberFilter()
    number_of_rooms__gt = django_filters.NumberFilter(field_name="number_of_rooms", lookup_expr="gt")
    number_of_rooms__lt = django_filters.NumberFilter(field_name="number_of_rooms", lookup_expr="lt")
    area = django_filters.NumberFilter()
    area__gt = django_filters.NumberFilter(field_name="area", lookup_expr="gt")
    area__lt = django_filters.NumberFilter(field_name="area", lookup_expr="lt")
    kitchen = django_filters.NumberFilter()
    kitchen__gt = django_filters.NumberFilter(field_name="kitchen", lookup_expr="gt")
    kitchen__lt = django_filters.NumberFilter(field_name="kitchen", lookup_expr="lt")
    features = django_filters.ModelMultipleChoiceFilter(queryset=Feature.objects.all())
    concession = django_filters.NumberFilter()
    concession__gt = django_filters.NumberFilter(field_name="concession", lookup_expr="gt")
    concession__lt = django_filters.NumberFilter(field_name="concession", lookup_expr="lt")
    deal_price = django_filters.NumberFilter()
    deal_price__gt = django_filters.NumberFilter(field_name="deal_price", lookup_expr="gt")
    deal_price__lt = django_filters.NumberFilter(field_name="deal_price", lookup_expr="lt")
    contract_price = django_filters.NumberFilter()
    contract_price__gt = django_filters.NumberFilter(field_name="contract_price", lookup_expr="gt")
    contract_price__lt = django_filters.NumberFilter(field_name="contract_price", lookup_expr="lt")
    base_price = django_filters.NumberFilter()
    base_price__gt = django_filters.NumberFilter(field_name="base_price", lookup_expr="gt")
    base_price__lt = django_filters.NumberFilter(field_name="base_price", lookup_expr="lt")
    contract_type = django_filters.ChoiceFilter(choices=Apartment.contract_types)

    class Meta:
        model = Apartment
        fields = ['id', 'commission', 'developer', 'residential_complex', 'queue', 'corpus', 'section',
                  'floor', 'floor_overall', 'deadline', 'number_of_rooms', 'area', 'kitchen', 'features',
                  'concession', 'deal_price', 'contract_price', 'base_price', 'contract_type']


class BSResidentialComplexForm(BSModalModelForm):
    class Meta:
        model = ResidentialComplex

        fields = [
            'complex_name',
            'developer'
        ]

        labels = {
            'comlex_name': 'Наименование',
            'developer': 'Застройщик'
        }

        widgets = {
            'developer': select2forms.Select2Widget()
        }


class ResidentialComplexForm(forms.ModelForm):
    developer = forms.ModelChoiceField(Developer.objects.values_list('short_name', flat=True))

    class Meta:
        model = ResidentialComplex

        fields = [
            'complex_name',
            'developer'
        ]

        widgets = {
            'complex_name': forms.TextInput(attrs={'class': 'form-control',
                                                   'id': '#residential_complex_name',
                                                   'required': True}),
        }

        labels = {
            'comlex_name': 'Наименование',
            'developer': 'Застройщик'
        }


class BSPersonForm(BSModalModelForm):
    class Meta:
        model = Person

        fields = [
            'first_name',
            'middle_name',
            'last_name',
            'phone_number',
            'e_mail'
        ]

        labels = {
            'first_name': 'Имя',
            'middle_name': 'Отчество',
            'last_name': 'Фамилия',
            'phone_number': 'Номер телефона',
            'e_mail': 'Email'
        }

        widgets = {
            'first_name': forms.TextInput(attrs={'class': 'form-control',
                                                 'id': '#person_first_name',
                                                 'required': True}),
            'last_name': forms.TextInput(attrs={'class': 'form-control',
                                                'id': '#person_last_name',
                                                'required': True}),
            'middle_name': forms.TextInput(attrs={'class': 'form-control',
                                                  'id': '#person_middle_name',
                                                  'required': False}),
            'phone_number': forms.TextInput(attrs={'class': 'form-control',
                                                   'id': '#person_phone_number',
                                                   'required': False}),
            'e_mail': forms.EmailInput(attrs={'class': 'form-control',
                                              'id': '#person_email',
                                              'required': False})
        }


class PersonForm(forms.ModelForm):
    class Meta:
        model = Person

        fields = [
            'first_name',
            'middle_name',
            'last_name',
            'phone_number',
            'e_mail'
        ]

        labels = {
            'first_name': 'Имя',
            'middle_name': 'Отчество',
            'last_name': 'Фамилия',
            'phone_number': 'Номер телефона',
            'e_mail': 'Email'
        }

        widgets = {
            'first_name': forms.TextInput(attrs={'class': 'form-control',
                                                 'id': '#person_first_name',
                                                 'required': True}),
            'last_name': forms.TextInput(attrs={'class': 'form-control',
                                                'id': '#person_last_name',
                                                'required': True}),
            'middle_name': forms.TextInput(attrs={'class': 'form-control',
                                                  'id': '#person_middle_name',
                                                  'required': False}),
            'phone_number': forms.TextInput(attrs={'class': 'form-control',
                                                   'id': '#person_phone_number',
                                                   'required': False}),
            'e_mail': forms.EmailInput(attrs={'class': 'form-control',
                                              'id': '#person_email',
                                              'required': False})
        }


class BSBankForm(BSModalModelForm):
    class Meta:
        model = Bank

        fields = [
            'full_name',
            'short_name',
            'bik'
        ]

        labels = {
            'full_name': 'Полное наименование',
            'short_name': 'Краткое наименование',
            'bik': 'БИК'
        }

        widgets = {
            'full_name': forms.TextInput(attrs={'class': 'form-control',
                                                'id': "#bank_full_name",
                                                'required': False}),
            'short_name': forms.TextInput(attrs={'class': 'form-control',
                                                 'id': '#bank_short_name',
                                                 'required': True}),
            'bik': forms.TextInput(attrs={'class': 'form-control',
                                          'id': '#bank_bik',
                                          'required': False})
        }


class BankForm(forms.ModelForm):
    class Meta:
        model = Bank

        fields = [
            'full_name',
            'short_name',
            'bik'
        ]

        labels = {
            'full_name': 'Полное наименование',
            'short_name': 'Краткое наименование',
            'bik': 'БИК'
        }

        widgets = {
            'full_name': forms.TextInput(attrs={'class': 'form-control',
                                                'id': "#bank_full_name",
                                                'required': False}),
            'short_name': forms.TextInput(attrs={'class': 'form-control',
                                                 'id': '#bank_short_name',
                                                 'required': True}),
            'bik': forms.TextInput(attrs={'class': 'form-control',
                                          'id': '#bank_bik',
                                          'required': False})
        }


class BSFeatureForm(BSModalModelForm):
    class Meta:
        model = Feature
        fields = [
            'feature_name'
        ]

        labels = {
            'feature_name': 'Наименование'
        }

        widgets = {
            'feature_name': forms.TextInput(attrs={'class': 'form-control',
                                                   'id': "#feature_name",
                                                   'required': True})
        }


class FeatureForm(forms.ModelForm):
    class Meta:
        model = Feature
        fields = [
            'feature_name'
        ]

        labels = {
            'feature_name': 'Наименование'
        }

        widgets = {
            'feature_name': forms.TextInput(attrs={'class': 'form-control',
                                                   'id': "#feature_name",
                                                   'required': True})
        }


class BSDeveloperForm(BSModalModelForm):
    class Meta:
        model = Developer

        fields = [
            'full_name',
            'short_name',

            'legal_address',
            'fact_address',

            'inn',
            'kpp',
            'ogrn',
            'director',

            'phone_number',
            'web_site',
            'e_mail',

            'objects_done',
            'objects_progress',
            'active'
        ]

        labels = {
            'full_name': 'Полное наименование',
            'short_name': 'Краткое наименование',
            'legal_address': 'Юридический адрес',
            'fact_address': 'Фактический адрес',
            'inn': 'ИНН',
            'kpp': 'КПП',
            'ogrn': 'ОГРН',
            'director': 'Руководитель',
            'phone_number': 'Номер телефона',
            'web_site': 'Сайт',
            'e_mail': 'Email',
            'objects_done': 'Объектов сдано',
            'objects_progress': 'Объектов в строительстве'
        }

        widgets = {
            'full_name': forms.TextInput(attrs={'class': "form-control",
                                                'id': "#shortname",
                                                'required': False}),
            'short_name': forms.TextInput(attrs={'class': "form-control",
                                                 'id': "#shortname",
                                                 'required': True}),

            'legal_address': forms.TextInput(attrs={'class': "form-control",
                                                    'id': "#legaladdr",
                                                    'required': False}),
            'fact_address': forms.TextInput(attrs={'class': "form-control",
                                                   'id': "#factaddr",
                                                   'required': False}),

            'inn': forms.TextInput(attrs={'class': "form-control",
                                          'id': "#developerInn",
                                          'required': False}),
            'kpp': forms.TextInput(attrs={'class': "form-control",
                                          'id': "#developerKpp",
                                          'required': False}),
            'ogrn': forms.TextInput(attrs={'class': "form-control",
                                           'id': "#developerOgrn",
                                           'required': False}),

            'director': forms.TextInput(attrs={'class': "form-control",
                                               'id': "#director",
                                               'required': False}),
            'phone_number': forms.TextInput(attrs={'class': "form-control",
                                                   'id': "#phoneNumber",
                                                   'required': False}),
            'web_site': forms.TextInput(attrs={'class': "form-control",
                                               'id': "#webSite",
                                               'required': False}),
            'e_mail': forms.TextInput(attrs={'class': "form-control",
                                             'id': "#devemail",
                                             'required': False}),

            'objects_done': forms.TextInput(attrs={'class': "form-control",
                                                   'id': "#objdone",
                                                   'required': False}),
            'objects_progress': forms.TextInput(attrs={'class': "form-control",
                                                       'id': "#objprogress",
                                                       'required': False})
        }


class DeveloperForm(forms.ModelForm):
    class Meta:
        model = Developer

        fields = [
            'full_name',
            'short_name',

            'legal_address',
            'fact_address',

            'inn',
            'kpp',
            'ogrn',
            'director',

            'phone_number',
            'web_site',
            'e_mail',

            'objects_done',
            'objects_progress',
            'active'
        ]

        labels = {
            'full_name': 'Полное наименование',
            'short_name': 'Краткое наименование',
            'legal_address': 'Юридический адрес',
            'fact_address': 'Фактический адрес',
            'inn': 'ИНН',
            'kpp': 'КПП',
            'ogrn': 'ОГРН',
            'director': 'Руководитель',
            'phone_number': 'Номер телефона',
            'web_site': 'Сайт',
            'e_mail': 'Email',
            'objects_done': 'Объектов сдано',
            'objects_progress': 'Объектов в строительстве'
        }

        widgets = {
            'full_name': forms.TextInput(attrs={'class': "form-control",
                                                'id': "#shortname",
                                                'required': False}),
            'short_name': forms.TextInput(attrs={'class': "form-control",
                                                 'id': "#shortname",
                                                 'required': True}),

            'legal_address': forms.TextInput(attrs={'class': "form-control",
                                                    'id': "#legaladdr",
                                                    'required': False}),
            'fact_address': forms.TextInput(attrs={'class': "form-control",
                                                   'id': "#factaddr",
                                                   'required': False}),

            'inn': forms.TextInput(attrs={'class': "form-control",
                                          'id': "#developerInn",
                                          'required': False}),
            'kpp': forms.TextInput(attrs={'class': "form-control",
                                          'id': "#developerKpp",
                                          'required': False}),
            'ogrn': forms.TextInput(attrs={'class': "form-control",
                                           'id': "#developerOgrn",
                                           'required': False}),

            'director': forms.TextInput(attrs={'class': "form-control",
                                               'id': "#director",
                                               'required': False}),
            'phone_number': forms.TextInput(attrs={'class': "form-control",
                                                   'id': "#phoneNumber",
                                                   'required': False}),
            'web_site': forms.TextInput(attrs={'class': "form-control",
                                               'id': "#webSite",
                                               'required': False}),
            'e_mail': forms.TextInput(attrs={'class': "form-control",
                                             'id': "#devemail",
                                             'required': False}),

            'objects_done': forms.TextInput(attrs={'class': "form-control",
                                                   'id': "#objdone",
                                                   'required': False}),
            'objects_progress': forms.TextInput(attrs={'class': "form-control",
                                                       'id': "#objprogress",
                                                       'required': False})
        }


class BSApartmentForm(BSModalModelForm):

    class Meta:
        model = Apartment

        fields = [
            'developer', 'contract_type', 'corpus', 'area', 'floor_overall', 'adversary_source', 'deal_price',
            'bank', 'full_sum_in_contract', 'merchant', 'internal_number', 'start_sale_date', 'actualization_date',
            'deadline', 'residential_complex', 'section', 'kitchen', 'fact_price_on_hands', 'commission',
            'concession', 'remainder', 'compensation_payer', 'works_with_agent', 'creator', 'queue_number',
            'number_of_rooms', 'floor', 'contract_price', 'sub_agent_commission', 'mortgage_fit',
            'seller_payed_for_mortgage', 'documents', 'goal', 'base_price', 'features', 'photo', 'plan'
        ]

        labels = {
            'developer': 'Застройщик', 'contract_type': 'Тип договора', 'corpus': 'Корпус', 'area': 'Площадь',
            'floor_overall': 'Этажность', 'adversary_source': 'Источник', 'deal_price': 'Цена сделки',
            'bank': 'Банк (обременение)', 'full_sum_in_contract': 'Полная цена в договоре', 'merchant': 'Продавец',
            'internal_number': 'Внутренний номер', 'start_sale_date': 'Дата начала продажи',
            'actualization_date': 'Дата актуализации', 'deadline': 'Срок сдачи',
            'residential_complex': 'Жилой комплекс', 'section': 'Секция', 'kitchen': 'Кухня',
            'fact_price_on_hands': 'Цена на руки', 'commission': 'Комиссия', 'concession': 'Уступка',
            'remainder': 'Остаток', 'compensation_payer': 'Кто платит компенсацию', 'works_with_agent': 'Агент',
            'creator': 'Пользователь, создавший запись', 'queue_number': 'Номер очереди',
            'number_of_rooms': 'Количество комнат', 'floor': 'Этаж', 'contract_price': 'Цена в договоре',
            'sub_agent_commission': 'Комиссия субагенту', 'mortgage_fit': 'Ипотека',
            'seller_payed_for_mortgage': 'Погашение ипотеки', 'documents': 'Документы', 'goal': 'Цель продажи',
            'base_price': 'Базовая цена', 'features': 'Особенности', 'photo': 'Фото квартиры',
            'plan': 'План квартиры'
        }

        widgets = {

            'developer': Select2(attrs={'class': 'form-control form-control-sm developers', 'id': 'developers'}),
            'residential_complex': Select2(
                attrs={'class': 'form-control form-control-sm residential_complexes', 'id': 'residential_complexes'}),
            'bank': forms.Select(attrs={'class': 'form-control form-control-sm banks', 'id': 'banks'}),
            'contract_type': forms.Select(attrs={'class': 'form-control form-control-sm', 'id': '#contract_type'}),
            'corpus': forms.NumberInput(attrs={'class': 'form-control form-control-sm', 'id': '#corpus'}),

            'area': forms.NumberInput(attrs={'class': 'form-control form-control-sm', 'id': '#area'}),
            'floor_overall': forms.NumberInput(attrs={'class': 'form-control form-control-sm', 'id': '#floor_overall'}),
            'adversary_source': forms.Select(attrs={'class': 'form-control form-control-sm', 'id': '#adversary_source'}),
            'deal_price': forms.NumberInput(attrs={'class': 'form-control form-control-sm', 'id': '#deal_price'}),
            'full_sum_in_contract': forms.Select(attrs={'class': 'form-control form-control-sm', 'id': '#full_sum_in_contract'}),
            'merchant': forms.Select(attrs={'class': 'form-control form-control-sm', 'id': '#merchant'}),
            'internal_number': forms.NumberInput(attrs={'class': 'form-control form-control-sm', 'id': '#internal_number'}),
            'section': forms.NumberInput(attrs={'class': 'form-control form-control-sm', 'id': '#section'}),
            'kitchen': forms.NumberInput(attrs={'class': 'form-control form-control-sm', 'id': '#kitchen'}),
            'fact_price_on_hands': forms.NumberInput(attrs={'class': 'form-control form-control-sm', 'id': '#fact_price_on_hands'}),
            'commission': forms.NumberInput(attrs={'class': 'form-control form-control-sm', 'id': '#commission'}),
            'concession': forms.NumberInput(attrs={'class': 'form-control form-control-sm', 'id': '#concession'}),
            'remainder': forms.NumberInput(attrs={'class': 'form-control form-control-sm', 'id': '#remainder'}),
            'compensation_payer': forms.Select(attrs={'class': 'form-control form-control-sm', 'id': '#compensation_payer'}),
            'works_with_agent': forms.Select(attrs={'class': 'form-control form-control-sm', 'id': '#works_with_agent'}),
            'creator': forms.Select(attrs={'class': 'form-control form-control-sm', 'id': '#creator'}),
            'queue_number': forms.NumberInput(attrs={'class': 'form-control form-control-sm', 'id': '#queue_number'}),
            'number_of_rooms': forms.NumberInput(attrs={'class': 'form-control form-control-sm', 'id': '#number_of_rooms'}),
            'floor': forms.NumberInput(attrs={'class': 'form-control form-control-sm', 'id': '#floor'}),
            'contract_price': forms.NumberInput(attrs={'class': 'form-control form-control-sm', 'id': '#contract_price'}),
            'sub_agent_commission': forms.NumberInput(attrs={'class': 'form-control form-control-sm', 'id': '#sub_agent_commission'}),
            'mortgage_fit': forms.Select(attrs={'class': 'form-control form-control-sm', 'id': '#mortgage_fit'}),
            'seller_payed_for_mortgage': forms.Select(
                attrs={'class': 'form-control form-control-sm', 'id': '#seller_payed_for_mortgage'}),

            'documents': forms.Select(attrs={'class': 'form-control form-control-sm', 'id': '#documents'}),
            'goal': forms.Select(attrs={'class': 'form-control form-control-sm', 'id': '#goal'}),
            'base_price': forms.NumberInput(attrs={'class': 'form-control form-control-sm', 'id': '#base_price'}),

            'photo': forms.FileInput(attrs={'class': 'form-control form-control-sm', 'id': '#photo'}),
            'plan': forms.FileInput(attrs={'class': 'form-control form-control-sm', 'id': '#plan'}),
            'features': forms.CheckboxSelectMultiple(attrs={'class': 'form-control form-control-sm'}),

            'start_sale_date': forms.DateInput(attrs={'class': 'form-control form-control-sm',
                                                      'type': 'date'}),
            'deadline': forms.DateInput(attrs={'class': 'form-control form-control-sm',
                                               'type': 'date'}),
            'actualization_date': forms.DateInput(attrs={'class': 'form-control form-control-sm',
                                                         'type': 'date'}),
        }


