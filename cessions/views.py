from django.shortcuts import render, redirect
from .forms import DeveloperForm, FeatureForm, BankForm, ResidentialComplexForm, PersonForm, \
    BSResidentialComplexForm, BSDeveloperForm, BSFeatureForm, BSPersonForm, BSBankForm, BSApartmentForm, \
    ApartmentFilter
from .models import Developer, Bank, ResidentialComplex, Apartment, Feature, Person
from bootstrap_modal_forms.generic import BSModalCreateView, BSModalUpdateView, BSModalDeleteView, BSModalReadView
from bootstrap_modal_forms.mixins import PopRequestMixin
from django.urls import reverse_lazy
from django_datatables_view.base_datatable_view import BaseDatatableView
from .tables import ApartmentTable
from django_tables2 import SingleTableView
from django.core.files.storage import FileSystemStorage
from django.views.generic import View

# class Filter(BSModalFormView):
#     template_name = "apartments/filter_apartments.html"
#     form_class = ApartmentFilter

#
# def upload(request):
#
#     urls = {'photo': None, 'plan': None}
#     if request.method == 'POST' and request.FILES['photo']:
#         photo = request.FILES['photo']
#         file_system_storage = FileSystemStorage()
#         file = file_system_storage.save(photo.name, photo)
#         urls['photo'] = file_system_storage.url(file)
#     if request.method == 'POST' and request.FILES['plan']:
#         plan = request.FILES['plan']
#         file_system_storage = FileSystemStorage()
#         file = file_system_storage.save(plan.name, plan)
#         urls['plan'] = file_system_storage.url(file)
#
#
#     return render (request, 'upload.html', {'file_urls': urls})
#
#


class ApartmentsListView(SingleTableView):
    model = Apartment
    table_class = ApartmentTable
    template_name = 'apartments/apartments.html'


class ApartmentInfoView(BSModalReadView):
    model = Apartment
    template_name = "apartments/modal_apartment_info.html"


class ApartmentsColumnVisibility(BSModalReadView):
    template_name = "apartments/column_visibility.html"


class CreateResidentialComplexView(BSModalCreateView):
    template_name = 'references/residential_complexes/modal_add_residential_complex.html'
    form_class = BSResidentialComplexForm
    success_message = 'Success: Residential Complex created'
    success_url = reverse_lazy('residential_complexes_list')


class EditResidentialComplexView(BSModalUpdateView):
    model = ResidentialComplex
    template_name = 'references/residential_complexes/modal_edit_residential_complex.html'
    form_class = BSResidentialComplexForm
    success_message = 'Success: Residential Complex was updated'
    success_url = reverse_lazy('residential_complexes_list')


class DeleteResidentialComplexView(BSModalDeleteView):
    model = ResidentialComplex
    template_name = 'references/residential_complexes/modal_delete_residential_complex.html'
    success_message = 'Success: Residential Complex was deleted'
    success_url = reverse_lazy('residential_complexes_list')


class CreateDeveloperView(BSModalCreateView):
    template_name = 'references/developers/modal_create_developer.html'
    form_class = BSDeveloperForm
    success_message = 'Success: Developer created'
    success_url = reverse_lazy('developers')


class EditDeveloperView(BSModalUpdateView):
    model = Developer
    template_name = 'references/developers/modal_update_developer.html'
    form_class = BSDeveloperForm
    success_message = 'Success: Developer was updated'
    success_url = reverse_lazy('developers')


class DeleteDeveloperView(BSModalDeleteView):
    model = Developer
    template_name = 'references/developers/modal_delete_developer.html'
    success_message = 'Success: Developer was deleted'
    success_url = reverse_lazy('developers')


class CreateFeatureView(BSModalCreateView):
    template_name = 'references/features/modal_create_feature.html'
    form_class = BSFeatureForm
    success_message = 'Success: Feature created'
    success_url = reverse_lazy('features')


class EditFeatureView(BSModalUpdateView):
    model = Feature
    template_name = 'references/features/modal_update_feature.html'
    form_class = BSFeatureForm
    success_message = 'Success: Feature was updated'
    success_url = reverse_lazy('features')


class DeleteFeatureView(BSModalDeleteView):
    model = Feature
    template_name = 'references/features/modal_delete_feature.html'
    success_message = 'Success: Feature was deleted'
    success_url = reverse_lazy('features')


class CreatePersonView(BSModalCreateView):
    template_name = 'references/persons/modal_create_person.html'
    form_class = BSPersonForm
    success_message = 'Success: Person created'
    success_url = reverse_lazy('persons')


class EditPersonView(BSModalUpdateView):
    model = Person
    template_name = 'references/persons/modal_update_person.html'
    form_class = BSPersonForm
    success_message = 'Success: Person was updated'
    success_url = reverse_lazy('persons')


class DeletePersonView(BSModalDeleteView):
    model = Person
    template_name = 'references/persons/modal_delete_person.html'
    success_message = 'Success: Person was deleted'
    success_url = reverse_lazy('persons')


class CreateBankView(BSModalCreateView):
    template_name = 'references/banks/modal_create_bank.html'
    form_class = BSBankForm
    success_message = 'Success: Bank created'
    success_url = reverse_lazy('banks')


class EditBankView(BSModalUpdateView):
    model = Bank
    template_name = 'references/banks/modal_update_bank.html'
    form_class = BSBankForm
    success_message = 'Success: Bank was updated'
    success_url = reverse_lazy('banks')


class DeleteBankView(BSModalDeleteView):
    model = Bank
    template_name = 'references/banks/modal_delete_bank.html'
    success_message = 'Success: Bank was deleted'
    success_url = reverse_lazy('banks')


class CreateApartmentView(BSModalCreateView):

    template_name = 'apartments/modal_create_apartment.html'
    form_class = BSApartmentForm
    success_message = 'Success: Apartment created'
    success_url = reverse_lazy('apartments')


class EditApartmentView(BSModalUpdateView, ):

    model = Apartment
    template_name = 'apartments/modal_update_apartment.html'
    form_class = BSApartmentForm
    success_message = 'Success: Apartment was updated'
    success_url = reverse_lazy('apartments')


class DeleteApartmentView(BSModalDeleteView):
    model = Apartment
    template_name = 'apartments/modal_delete_apartment.html'
    success_message = 'Success: Apartment was deleted'
    success_url = reverse_lazy('apartments')


def cover_view(request):
    return render(request, "registration/cover.html")


def apartment_info_view(request):
    return render(request, "apartments/modal_apartment_info.html")


def features_view(request):
    features = Feature.objects.all()
    return render(request,
                  "references/features/features.html",
                  {'features': features})


def developers_view(request):
    developers = Developer.objects.all()
    return render(request,
                  "references/developers/developers.html",
                  {'developers': developers})


def banks_view(request):
    banks = Bank.objects.all()
    return render(request,
                  "references/banks/banks.html",
                  {'banks': banks})


def residential_complex_view(request):
    residential_complexes = ResidentialComplex.objects.all()
    return render(request,
                  "references/residential_complexes/residential_complexes.html",
                  {'residential_complexes': residential_complexes})


def apartment_view(request):
    apartments = Apartment.objects.all()
    return render(request,
                  "apartments/apartments.html",
                  {'apartments': apartments})


def person_view(request):
    persons = Person.objects.all()
    return render(request,
                  "references/persons/persons.html",
                  {'persons': persons})


def add_developer_record(request):
    if request.method == "POST":
        form = DeveloperForm(request.POST)
        if form.is_valid():
            try:
                form.save()
                return redirect('/developers')
            except:
                pass
    else:
        form = DeveloperForm()
    return render(request, 'references/developers/add_developer.html', {'form': form})


def add_residential_complex_record(request):
    if request.method == "POST":
        form = ResidentialComplexForm(request.POST)
        if form.is_valid():
            try:
                form.save()
                return redirect('/residential_complexes')
            except:
                pass
    else:
        form = ResidentialComplexForm()
    return render(request, 'references/residential_complexes/add_residential_complex.html', {'form': form})


def add_person_record(request):
    if request.method == "POST":
        form = PersonForm(request.POST)
        if form.is_valid():
            try:
                form.save()
                return redirect('/developers')
            except:
                pass
    else:
        form = PersonForm()
    return render(request, 'references/persons/add_person.html', {'form': form})


def add_feature_record(request):
    if request.method == "POST":
        form = FeatureForm(request.POST)
        if form.is_valid():
            try:
                form.save()
                return redirect('/features')
            except:
                pass
    else:
        form = FeatureForm()
    return render(request, 'references/features/add_feature.html', {'form': form})


def add_bank_record(request):
    if request.method == "POST":
        form = BankForm(request.POST)
        if form.is_valid():
            try:
                form.save()
                return redirect('/banks')
            except:
                pass
    else:
        form = BankForm()
    return render(request, 'references/banks/add_bank.html', {'form': form})


def apartment_list(request):
    f = ApartmentFilter(request.GET, queryset=Apartment.objects.all())
    return render(request, 'apartments/apartments.html', {'filter': f})
