from cessions.models import Apartment
import django_tables2 as tables
from django_tables2.utils import A


class ApartmentTable(tables.Table):

    checked = tables.CheckBoxColumn(accessor="pk", verbose_name="")
    id = tables.Column(accessor='pk', verbose_name="ID")
    edit = tables.TemplateColumn(template_name="apartments/operations.html",
                                 verbose_name="")

    commission_info = tables.Column(accessor="commission_info", verbose_name="Комиссия")
    developer_info = tables.Column(accessor="developer_info", verbose_name="Застройщик/ЖК")
    area_info = tables.Column(accessor="area_info", verbose_name="Метраж")
    building_info = tables.Column(accessor="building_info", verbose_name="Здание")
    price_info = tables.Column(accessor="price_info", verbose_name="Цена")
    deadline_info = tables.DateColumn(accessor="deadline", verbose_name="Срок сдачи",
                                      format='d/m/Y')

    class Meta:
        model = Apartment
        fields = ('checked', 'id', 'commission_info', 'developer_info', 'building_info', 'deadline_info',
                  'area_info', 'concession', 'price_info', 'contract_type', 'edit',)
        attrs = {"class": "table table-hover table-bordered table-light"}