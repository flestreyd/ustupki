from django.db import models
from django.utils.translation import gettext_lazy as _
# from django.contrib.auth.models import User
from django.contrib.auth.models import AbstractUser
from django.utils.safestring import mark_safe


class CessionUser(AbstractUser):

    cession_user_first_name = models.CharField(max_length=256,
                                               null=False,
                                               blank=False,
                                               verbose_name="Имя")
    cession_user_patronym = models.CharField(max_length=256,
                                             null=True,
                                             blank=True,
                                             verbose_name="Отчество")
    cession_user_last_name = models.CharField(max_length=256,
                                              null=False,
                                              blank=False,
                                              verbose_name="Фамилия")

    cession_user_phone_number = models.CharField(max_length=12,
                                                 null=False,
                                                 blank=False,
                                                 verbose_name="Номер телефона")
    cession_user_email = models.EmailField(null=True,
                                           max_length=128,
                                           blank=True,
                                           verbose_name="Электронная почта")

    ADMIN = 1
    CREATOR = 2
    CURATOR = 3
    LAWYER = 4
    SUPERVISOR = 5

    ROLE_CHOICES = (
        (ADMIN, 'ADMIN'),
        (CREATOR, 'CREATOR'),
        (CURATOR, 'CURATOR'),
        (LAWYER, 'LAWYER'),
        (SUPERVISOR, 'SUPERVISOR')
    )

    role = models.PositiveSmallIntegerField(choices=ROLE_CHOICES, blank=True, null=True)


class DeveloperManager(models.Manager):
    def create_developer(self, short_name):
        self.create(short_name=short_name)
        return self


class BankManager(models.Manager):
    def create_bank(self, short_name):
        self.create(short_name=short_name)
        return self


class ApartmentManager(models.Manager):
    def create_apartment(self):
        self.create()
        return self


class DealManager(models.Manager):
    def create_deal(self):
        self.create()
        return self


class ResidentialComplexManager(models.Manager):
    def create_residential_complex(self):
        self.create()
        return self


class PersonManager(models.Manager):
    def create_person(self):
        self.create()
        return self


class FeatureManager(models.Manager):
    def create_feature(self):
        self.create()
        return self


class Developer(models.Model):
    """Застройшик"""

    # Names
    full_name = models.CharField(max_length=1024,
                                 null=True,
                                 blank=True,
                                 verbose_name="Developer's full name")
    short_name = models.CharField(max_length=512,
                                  null=False,
                                  db_index=True,
                                  verbose_name="Developer's short name")

    # address information
    legal_address = models.CharField(max_length=1024,
                                     null=True,
                                     blank=True,
                                     verbose_name="Developer's legal address")
    fact_address = models.CharField(max_length=1024,
                                    null=True,
                                    blank=True,
                                    verbose_name="Developer's fact address")

    # attributes
    inn = models.IntegerField(null=True,
                              blank=True,
                              verbose_name="Developer's inn code")
    kpp = models.IntegerField(null=True,
                              blank=True,
                              verbose_name="Developer's kpp code")
    ogrn = models.IntegerField(null=True,
                               blank=True,
                               verbose_name="Developer's ogrn code")

    # company contact data
    director = models.CharField(max_length=512,
                                null=True,
                                blank=True,
                                verbose_name="Director's full name")
    phone_number = models.CharField(max_length=64,
                                    null=True,
                                    blank=True,
                                    verbose_name="Company's contact phone number")
    web_site = models.CharField(max_length=512,
                                null=True,
                                blank=True,
                                verbose_name="Company's website page address")
    e_mail = models.EmailField(max_length=64,
                               null=True,
                               blank=True,
                               verbose_name="Company's contact e-mail")

    # statistical data
    objects_done = models.IntegerField(null=True,
                                       blank=True,
                                       verbose_name="Objects finished by developer")
    objects_progress = models.IntegerField(null=True,
                                           blank=True,
                                           verbose_name="Objects processed by developer")
    logo = models.ImageField(null=True,
                             blank=True,
                             verbose_name="Image with logo")

    active = models.BooleanField(default=True)
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

    objects = DeveloperManager()

    def __str__(self):
        return self.short_name


class ResidentialComplex(models.Model):
    """жилой комплекс"""
    # Names

    complex_name = models.CharField(max_length=512,
                                    null=False,
                                    db_index=True,
                                    verbose_name="Residential complex name")

    developer = models.ForeignKey(to=Developer,
                                  on_delete=models.CASCADE,
                                  verbose_name='Developer',
                                  null=True,
                                  blank=True)

    created = models.DateTimeField(auto_now_add=True,
                                   verbose_name="Create timestamp")
    updated = models.DateTimeField(auto_now=True,
                                   verbose_name="Update timestamp")

    objects = ResidentialComplexManager()

    def __str__(self):
        return self.complex_name


class Bank(models.Model):
    """Данные о банке"""
    short_name = models.CharField(max_length=512,
                                  null=False,
                                  blank=False,
                                  verbose_name="Bank short name")
    full_name = models.CharField(max_length=1024,
                                 null=True,
                                 blank=True,
                                 verbose_name="Bank full name")
    bik = models.CharField(max_length=100,
                           null=True,
                           blank=True,
                           verbose_name="Bank bik code")

    created_by = models.ForeignKey(to=CessionUser,
                                   on_delete=models.CASCADE,
                                   null=True,
                                   verbose_name="Created by user")

    created = models.DateTimeField(auto_now_add=True,
                                   verbose_name="Create timestamp")
    updated = models.DateTimeField(auto_now=True,
                                   verbose_name="Update timestamp")

    objects = BankManager()

    def __str__(self):
        return self.short_name


class Feature(models.Model):
    """Особенности"""
    feature_name = models.CharField(max_length=512,
                                    null=False,
                                    blank=False,
                                    verbose_name='Feature descriptive name')

    created = models.DateTimeField(auto_now_add=True,
                                   verbose_name="object created timestamp")
    updated = models.DateTimeField(auto_now=True,
                                   verbose_name="object updated timestamp")

    active = models.BooleanField(default=True,
                                 null=True,
                                 blank=True,
                                 verbose_name='object is active')

    objects = FeatureManager()

    def __str__(self):
        feature_code_name = f"{self.id} : {self.feature_name}"
        return feature_code_name


class Apartment(models.Model):
    """Квартира и все атрибуты объекта"""

    YES = 'YES'
    NO = 'NO'

    yes_no_choice = [
        (YES, 'Да'),
        (NO, 'Нет')

    ]

    EXCLUSIVE = 'EXCLUSIVE'
    PARTNER = 'PARTNER'
    OPEN_MARKET = 'OPEN_MARKET'

    adversary_source_choice = [
        (EXCLUSIVE, 'Эксклюзивная квартира'),
        (PARTNER, 'Партнерская квартира'),
        (OPEN_MARKET, 'Открытый рынок')
    ]

    COMPENSATION_FROM_SELLER = 'COMPENSATION_FROM_SELLER'
    COMPENSATION_FROM_BUYER = 'COMPENSATION_FROM_BUYER'
    COMPENSATION_FROM_BOTH = 'COMPENSATION_FROM_BOTH'

    compensation_payer_choice = [
        (COMPENSATION_FROM_BUYER, 'Покупатель'),
        (COMPENSATION_FROM_SELLER, 'Продавец'),
        (COMPENSATION_FROM_BOTH, '50/50')
    ]

    PRIVATE = 'PRIVATE'
    LEGAL = 'LEGAL'
    INDIVIDUAL = 'INDIVIDUAL'

    merchant_type = [
        (PRIVATE, 'Частное лицо'),
        (LEGAL, 'Юридическое лицо'),
        (INDIVIDUAL, 'Физическое лицо')
    ]

    INVEST_GOAL = 'INVEST_GOAL'
    NEW_APARTMENT = 'NEW_APARTMENT'
    MONEY = 'MONEY'

    goals = [
        (INVEST_GOAL, 'Продажа в качестве инвестиции'),
        (NEW_APARTMENT, 'Покупка нового жилья'),
        (MONEY, 'Срочная необходимость в денежных средствах')
    ]

    CESSION = 'Цессия'
    ANY = 'Другое'

    contract_types = [
        (CESSION, 'Цессия'),
        (ANY, 'Другое')
    ]

    """Объект продажи"""
    developer = models.ForeignKey(to=Developer,
                                  on_delete=models.CASCADE,
                                  null=False,
                                  blank=False,
                                  verbose_name="Застройщик")

    contract_type = models.CharField(choices=contract_types,
                                     max_length=32,
                                     null=False,
                                     blank=False,
                                     default=CESSION,
                                     verbose_name='Тип контракта')

    corpus = models.CharField(max_length=8,
                              null=True,
                              blank=False,
                              verbose_name="Корпус")

    area = models.DecimalField(max_digits=17,
                               decimal_places=2,
                               null=False,
                               blank=False,
                               verbose_name="Площадь")

    floor_overall = models.IntegerField(null=False,
                                        blank=False,
                                        verbose_name="Этажность")
    adversary_source = models.CharField(choices=adversary_source_choice,
                                        null=False,
                                        blank=False,
                                        max_length=64,
                                        default=OPEN_MARKET,
                                        verbose_name="Источник")
    deal_price = models.DecimalField(max_digits=17,
                                     decimal_places=2,
                                     null=False,
                                     blank=False,
                                     verbose_name='Цена сделки')

    bank = models.ForeignKey(to=Bank,
                             related_name="bank",
                             on_delete=models.CASCADE,
                             null=True,
                             blank=True,
                             verbose_name='Банк')
    full_sum_in_contract = models.CharField(choices=yes_no_choice,
                                            max_length=3,
                                            default=YES,
                                            verbose_name='Полная сумма')

    merchant = models.CharField(choices=merchant_type,
                                max_length=16,
                                default=PRIVATE,
                                blank=True,
                                verbose_name='Продавец')
    internal_number = models.IntegerField(null=False,
                                          blank=False,
                                          verbose_name="Номер")
    start_sale_date = models.DateTimeField(null=True,
                                           blank=True,
                                           verbose_name='Начало продажи')
    actualization_date = models.DateTimeField(null=True,
                                              blank=True,
                                              verbose_name='Дата актуализации')
    deadline = models.DateTimeField(verbose_name='Срок сдачи')
    residential_complex = models.ForeignKey(to=ResidentialComplex,
                                            on_delete=models.CASCADE,
                                            null=True,
                                            blank=True,
                                            verbose_name="ЖК")
    section = models.IntegerField(null=False,
                                  blank=False,
                                  verbose_name="Секция")
    kitchen = models.IntegerField(null=False,
                                  blank=False,
                                  verbose_name="Кухня")

    fact_price_on_hands = models.DecimalField(max_digits=17,
                                              decimal_places=2,
                                              default=0,
                                              null=False,
                                              blank=False,
                                              verbose_name='Цена на руки')

    commission = models.DecimalField(max_digits=4,
                                     decimal_places=2,
                                     null=False,
                                     blank=False,
                                     verbose_name='Комиссия')

    concession = models.CharField(max_length=10,
                                  null=False,
                                  verbose_name='Уступка')
    remainder = models.DecimalField(max_digits=17,
                                    decimal_places=2,
                                    null=False,
                                    blank=False,
                                    verbose_name='Остаток')
    compensation_payer = models.CharField(choices=compensation_payer_choice,
                                          max_length=64,
                                          default=COMPENSATION_FROM_BOTH,
                                          verbose_name='Компенсация')
    works_with_agent = models.CharField(choices=yes_no_choice,
                                        max_length=3,
                                        default=YES,
                                        verbose_name='Агент')
    creator = models.ForeignKey(to=CessionUser,
                                null=True,
                                blank=True,
                                related_name='apartment_creator_user',
                                on_delete=models.CASCADE)
    queue_number = models.IntegerField(null=True, verbose_name="Оч")
    number_of_rooms = models.IntegerField(null=True, verbose_name="Комнат")
    floor = models.IntegerField(null=True, verbose_name="Эт")
    contract_price = models.DecimalField(max_digits=17,
                                         decimal_places=2,
                                         null=False,
                                         blank=False,
                                         verbose_name='Цена в договоре')
    sub_agent_commission = models.DecimalField(max_digits=17,
                                               decimal_places=2,
                                               verbose_name='Комиссия субагенту')
    mortgage_fit = models.CharField(choices=yes_no_choice,
                                    max_length=3,
                                    default=YES,
                                    verbose_name='Ипотека')
    seller_payed_for_mortgage = models.CharField(choices=yes_no_choice,
                                                 max_length=3,
                                                 default=YES,
                                                 verbose_name='Гасит ли')
    documents = models.CharField(choices=yes_no_choice,
                                 max_length=3,
                                 default=YES,
                                 verbose_name='Документы')

    goal = models.CharField(choices=goals,
                            max_length=64,
                            default=NEW_APARTMENT,
                            verbose_name='Цель')

    base_price = models.DecimalField(max_digits=17,
                                     decimal_places=2,
                                     verbose_name='Цена рын')
    features = models.ManyToManyField(Feature,
                                      blank=True,
                                      null=True,
                                      verbose_name='Особенности')

    # additional
    photo = models.ImageField(null=True,
                              blank=True,
                              upload_to='apartment_photo',
                              verbose_name="Фото")
    plan = models.ImageField(null=True,
                             blank=True,
                             upload_to='apartment_plan',
                             verbose_name="План")
    # geo_location = models.ImageField(null=True, verbose_name="Geo location of object")
    # view_from_window = models.ImageField(null=True, verbose_name="Geo location window")
    created = models.DateTimeField(auto_now_add=True,
                                   verbose_name="object created timestamp")
    updated = models.DateTimeField(auto_now=True,
                                   verbose_name="object updated timestamp")
    objects = ApartmentManager()

    @property
    def commission_info(self):
        return mark_safe(f"{self.commission}%")

    @property
    def developer_info(self):
        return mark_safe(f"<strong class=\"text-success\">{self.developer.short_name}</strong> / {self.residential_complex.complex_name}")

    @property
    def area_info(self):
        return mark_safe(f"<strong class=\"text-success\">Комнат: </strong> {self.number_of_rooms}<br>"
                         f"<strong class=\"text-success\">Площадь: </strong> {self.area}м<sup>2</sup><br>"
                         f"<strong class=\"text-success\">Кухня: </strong>: {self.kitchen}м<sup>2</sup>")

    @property
    def building_info(self):
        return mark_safe(f"<strong class=\"text-success\">Очередь:</strong> {self.queue_number}<br>"
                         f"<strong class=\"text-success\">Корпус:</strong> {self.corpus}<br>"
                         f"<strong class=\"text-success\">Секция:</strong> {self.section}<br>"
                         f"<strong class=\"text-success\">Этаж:</strong> {self.floor} / {self.floor_overall}")

    @staticmethod
    def _format_price(price):
        if price >= 10 ** 6:
            price = price / 10 ** 6
        return mark_safe(f"{price}M")

    @property
    def price_info(self):
        return mark_safe(f"<strong class=\"text-success\">Сделка: </strong> {self._format_price(self.deal_price)}<br>"
                         f"<strong class=\"text-success\">Договор: </strong> {self._format_price(self.contract_price)}<br>"
                         f"<strong class=\"text-success\">Рыночная: </strong> {self._format_price(self.base_price)}")

    @property
    def operations_panel(self):
        return mark_safe(f"")

    def __str__(self):
        return str(self.id)


class Person(models.Model):
    first_name = models.CharField(blank=False,
                                  null=False,
                                  max_length=512,
                                  verbose_name='Customer first name')
    last_name = models.CharField(blank=False,
                                 null=False,
                                 max_length=512,
                                 verbose_name='Customer last name')
    middle_name = models.CharField(blank=True,
                                   null=True,
                                   max_length=512,
                                   verbose_name='Customer fathers name')

    phone_number = models.CharField(blank=True,
                                    null=True,
                                    max_length=64,
                                    verbose_name='Customer phone number')
    e_mail = models.EmailField(blank=True,
                               null=True,
                               verbose_name='Customer email')

    active = models.BooleanField(default=True)
    created = models.DateTimeField(auto_now=True)
    updated = models.DateTimeField(auto_now_add=True)

    objects = PersonManager()

    def __str__(self):
        fio = self.first_name + self.last_name + self.middle_name
        return fio


class Deal(models.Model):
    selling_object = models.ForeignKey(to=Apartment,
                                       related_name="selling_object",
                                       on_delete=models.CASCADE)
    customer = models.ForeignKey(to=Person,
                                 related_name="object_customer",
                                 on_delete=models.CASCADE)
    seller = models.ForeignKey(to=Person,
                               related_name="object_seller",
                               on_delete=models.CASCADE)

    CREATED = 'CREATED'
    DEPOSIT = 'DEPOSIT'
    LEGAL_DEAL = 'LEGAL'
    APPROVAL = 'APPROVAL'
    ADDITION = 'ADDITION'
    DEAL_EXECUTION = 'DEAL'
    COMMISSION = 'COMMISSION'
    CLOSED = 'CLOSED'

    DealStage = [
        (CREATED, 'curator add info about customer'),
        (DEPOSIT, 'curator add info about seller'),
        (LEGAL_DEAL, 'lawyer get deal and comment it'),
        (APPROVAL, 'lawyer send docs to sides'),
        (ADDITION, 'supervisor monitor addition'),
        (DEAL_EXECUTION, 'lawyer monitor deal and add docs'),
        (COMMISSION, 'supervisor pays commissions'),
        (CLOSED, 'supervisor close deal')
    ]

    deal_state = models.CharField(choices=DealStage,
                                  max_length=32,
                                  default=CREATED)

    owner = models.ForeignKey(to=CessionUser,
                              related_name='owner_user',
                              on_delete=models.CASCADE,
                              null=True)
    creator = models.ForeignKey(to=CessionUser,
                                related_name='creator_user',
                                on_delete=models.CASCADE,
                                null=True)

    documents = models.CharField(max_length=32000,
                                 blank=True,
                                 null=True,
                                 verbose_name="Documents of deal")
    comments = models.CharField(max_length=32000,
                                blank=True,
                                null=True,
                                verbose_name="Deal comments")

    active = models.BooleanField(default=True)
    created = models.DateTimeField(auto_now=True)
    updated = models.DateTimeField(auto_now_add=True)
    objects = DealManager()

    def __str__(self):
        return self.id
