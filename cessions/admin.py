from django.contrib import admin
from .models import Apartment


class ApartmentAdmin(admin.ModelAdmin):
    fields = ('developer', 'contract_type', 'corpus', 'area', 'floor_overall', 'adversary_source', 'deal_price',
              'bank', 'full_sum_in_contract', 'merchant', 'internal_number', 'start_sale_date', 'actualization_date',
              'deadline', 'residential_complex', 'section', 'kitchen', 'fact_price_on_hands', 'commission',
              'concession', 'remainder', 'compensation_payer', 'works_with_agent', 'creator', 'queue_number',
              'number_of_rooms', 'floor', 'contract_price', 'sub_agent_commission', 'mortgage_fit',
              'seller_payed_for_mortgage', 'documents', 'goal', 'base_price', 'features', 'photo', 'plan')


admin.site.register(Apartment, ApartmentAdmin)

# Register your models here.
